//Example usage in the command prompt
//node Server.js

// Parameters
const port = process.env.PORT || 3000; //Specify a port for our web server
const express = require('express'); //load express with the use of requireJs
const fetch = require("node-fetch")
const bodyParser = require('body-parser');
const app = express()
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json())

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

highScore = null;
credsList = {};
const projID = 'sisterproject-1';
let url = 'https://'+projID+'.firebaseio.com/highScore/.json';

getHTTP(url , (res)=>{
	highScore = res;
	if(highScore == null){
		highScore = {Computer1 : 100, Computer2 : 90,Computer3 : 80,Computer4 : 70,Computer5 : 60,
					 Computer6 : 50, Computer7 : 40,Computer8 : 30,Computer9 : 20,Computer10: 10}
					 
		saveHighScore();
	}
	console.log(highScore);
})


//loop to save memory
setInterval(loopEveryTwoHour, 7200000);
function loopEveryTwoHour() {
	let current = new Date();
	for (var key in credsList) {
		let delta = current - credsList[key];
		if(delta > 7200000){
			delete credsList[key];
		}
	}
}

app.get('/credentials' , (req,res) => {
	/*
	list = []
	for(var key in credsList){
		list.push(key);
	}
	*/
	res.send(credsList);
	
});


app.get('/playNewGame' , (req,res) => {
	
	let tempCreds = createNewCreds();
	credsList[tempCreds] = new Date();
	
	res.send(tempCreds);
	
});

app.post('/isInHighScore' , (req,res) => {
	
	let isNew = false;
	for(var key in highScore){
		if(req.body.score > highScore[key]){
			isNew = true;
			break;
		}
	}
	
	if(isNew){
		let current = new Date();
		credsList[req.body.creds] = current - credsList[req.body.creds];
		res.send(true);
	}else{
		if(credsList[req.body.creds] != undefined){
			delete credsList[req.body.creds];
		}
		res.send(false);
	}
});

//{creds : '' , userName : '' , score : 0 , timePassed:0 , stage: 5 , timePerStage : 0}
app.post('/newHighScore' , (req,res) => {
	
	//handle cheat here
	let isValid = checkCheat(req.body.creds , req.body.timePassed , req.body.stage , req.body.score , req.body.timePerStage , req.body.timeOffset);
	if(isValid.result == false){
		res.send(isValid);
		return;
	}
	
	//get actual highscore
	getHTTP(url , (actualHighscore)=>{ 
		highScore = actualHighscore;
		
		//check value score
		let pName = req.body.userName;
		let pScore = parseInt(req.body.score);//Math.random()*1000;
		let inserted = false;
		
		
		//Check is Name already exist
		if(highScore[pName] !=null  ){
			if( highScore[pName]  < pScore ){
				//end it here
				inserted = true;
				highScore[pName] = pScore;
			}else{
				res.send({result : false, message : "HighScore for "+pName+" was already higher than "+req.body.score+".", timeDiff : isValid.timeDiff});
				return;
			}
		}
		
		//sort to get highest score
		var sortable = [];
		for (var key in highScore) {
			sortable.push([key, highScore[key]]);
		}
		if(inserted == false){
			sortable.push([pName , pScore]);
		}

		sortable.sort(function(a, b) {
			return b[1] - a[1];
		});
		
		//get top 10 score
		sortable = sortable.slice(0,10);
		
		//refactor object
		highScore = {}
		for (let i = 0 ; i < sortable.length; i++) {
		  let temp = sortable[i];
		  highScore[temp[0]] = temp[1];
		}
		
		//save actual highscore
		saveHighScore(); 
		
		res.send({result : true, message : "HighScore for "+pName+" was saved!." , timeDiff : isValid.timeDiff});
	});
});


app.get('/reload' , (req,res) => {
	getHTTP(url , (actualHighscore)=>{ 
		highScore = actualHighscore;
		res.send("success");
	});
});


app.get('/highScore' , (req,res) => {
	sortable = [];
	for (var key in highScore) {
		sortable.push( highScore[key] );
	}
	res.send(sortable);
});

app.get('/highScoreName' , (req,res) => {
	res.send(highScore);
});


app.get('/' , (req,res) => {
	res.send("Welcome to Life Capcy Server");
});


//app.use(express.static(__dirname + '/'));//Serving static files
app.listen(port, function() { //Listener for specified port
    console.log("Server running at: http://localhost:" + port)
});

function getHTTP(url, callBack){
	
	fetch(url+"?format=export", {
		method: 'get'
	})
	.then(res => res.json())
	.then(json => {
		callBack(json);
	});
}

function putHTTP(url , body , callBack){
	
	fetch(url, {
		method: 'put',
		body:    JSON.stringify(body),
	})
	.then(res => res.json())
	.then(json => {
		callBack(json);
	});
}

function saveHighScore(){
	putHTTP(url , highScore , (result)=>{});
}

function createNewCreds() {
    return Math.random().toString(36).substr(2); // remove `0.`
};

function checkCheat(creds , timePassed , stage , score , timePerStage , timeOffset){
	
	// Time Check
	let lastPlayTime = credsList[creds];
	if(lastPlayTime == null){
		return {result : false, message : "ERROR : Trying to access database directly."};
	}
	
	let deltaTime = credsList[creds];
	delete credsList[creds];
	
	let diff =  Math.abs(deltaTime - timePassed);
	if( diff > 5000){
		return {result : false, message : "ERROR : Server connection time out." , timeDiff : (deltaTime - timePassed)};
	}
	
	if((stage * (stage+1)/2)*100 < score){
		return {result : false, message : "ERROR : Abnormal score detected." , timeDiff : (deltaTime - timePassed)};
	}
	
	if(timePerStage*stage + timeOffset < timePassed-5000){
		return {result : false, message : "ERROR : Abnormal time detected." , timeDiff : (deltaTime - timePassed)};
	}
	
	return {result : true, message : "Success : Valid data.: " , timeDiff : (deltaTime - timePassed)};
}
