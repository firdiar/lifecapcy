Guide = new Phaser.Class({
    Extends: Phaser.Scene,

	initialize:
	function Guide()
    {
        // Pembuatan scene baru
        Phaser.Scene.call(this, { key: 'guide' });
    },
	create:function(){
		
		if(this.clickSound==null){
			this.clickSound = this.sound.add('click');
			this.clickSound.setVolume(0.5);
		}
		
		this.loading = this.add.image(screenWidth/2 , screenHeight /2, 'loading');
		this.loading.setDepth(10);
		this.tweens.add({
            targets: this.loading,
            ease: "Power1",
            duration: 500,
            y: screenHeight /2 - screenHeight
        })
		
		this.add.image(screenWidth/2 , screenHeight /2, 'guide');
		
		this.btnStart = this.add.sprite(screenWidth * 0.1 , screenHeight *0.9, 'backBtn').setScale(0.5)
		 .setInteractive()
		 .on('pointerdown', () => {this.BackToMain(); this.clickSound.play();} )
		 .on('pointerover', () => { 
				this.btnStart.setFrame(1);  
				} )
		 .on('pointerout', () =>  { this.btnStart.setFrame(0);});
		
	},
	BackToMain:function(){
		this.tweens.add({
            targets: this.loading,
            ease: "Power1",
			onComplete : ()=> {this.scene.start('mainmenu', {test : 'empty'});},
            duration: 500,
            y: screenHeight/2
        })
	}
	
})