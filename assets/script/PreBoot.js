PreBoot = new Phaser.Class({
    Extends: Phaser.Scene,

	initialize:
	function PreBoot()
    {
        // Pembuatan scene baru
        Phaser.Scene.call(this, { key: 'preboot' });
    },
    preload: function() {
		this.load.image('loading', 'assets/image/LoadingScreen.png');
	},
	create:function(){
		
		this.add.image(screenWidth/2 , screenHeight /2, 'loading');
		this.scene.start('boot', {test : 'empty'});
		
	}
	
})