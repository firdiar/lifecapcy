


GameScene = new Phaser.Class({
    Extends: Phaser.Scene,

	initialize:
	function GameScene()
    {
        // Pembuatan scene baru
        Phaser.Scene.call(this, { key: 'gamescene' });
    },
	score : 0,
	obstacles : [],
	loading : null,
	addObsTimeEvery : 25000,
	timeCooldown : 9000,
	counterObs : 0,
	stage : 1,
	credential : '',
	create : function(){
		
		if(this.bgSound1 == null){
			this.bgSound1 = this.sound.add('soundGame1');
			this.bgSound1.loop = true;
			
		}
		this.bgSound1.setVolume(0.5);
		if(this.bgSound2 == null){
			this.bgSound2 = this.sound.add('soundGame2');
			this.bgSound2.loop = true;
			
		}
		this.bgSound2.setVolume(0.5);
		if(this.bgSound3 == null){
			this.bgSound3 = this.sound.add('soundGame3');
			this.bgSound3.loop = true;
			
		}
		this.bgSound3.setVolume(0.5);
		
        this.bgSound1.play();
		
		if(this.dieSound == null){
			this.dieSound = this.sound.add('soundDie');
		}
		
		if(this.clearSound == null){
			this.clearSound = this.sound.add('soundClear');
		}
		
		
		
		
		
		

		//init
		console.log("gamescene")
		this.score = 0;
		this.obstacle = [];
		this.secLooper = -2000;
		this.isFinished = false;
		this.xObs = 2;
		this.yObs = 2;
		this.fWave = 2;
		this.stage = 1;
		this.timePassed  = new Date().getTime();
		this.isStarted = false;
		
		this.counterObs = this.addObsTimeEvery;
		
		
		this.loading = this.add.image(screenWidth/2 , screenHeight /2, 'loading');
		this.loading.setDepth(10);
		
		
		
		
		//add background
        let bg = this.add.image(screenWidth/2 , screenHeight /2, 'bg');
		bg.setScale(1.2);
		
		
		this.scoreText = this.add.text(25,20, 'Score : 0', { fontFamily: 'Impact' , font: '"Press Start 2P"' });
		this.scoreText.setFontSize(25);
		
		this.stageText = this.add.text(160,20, 'Stage : 1', { fontFamily: 'Impact' , font: '"Press Start 2P"' });
		this.stageText.setFontSize(25);
		
		this.waveText = this.add.text(960,20, 'Final wave start in : 0', { fontFamily: 'Impact' , font: '"Press Start 2P"' });
		this.waveText.setFontSize(25);
		
		this.stageHeadingText = this.add.text(screenWidth/2 , screenHeight/2, '', { fontFamily: 'Impact' , font: '"Press Start 2P"' });
		this.stageHeadingText.setOrigin(0.5 , 0.5);
		this.stageHeadingText.setFontSize(100);
		this.stageHeadingText.setDepth(5);
		
		
		this.capcy = this.createCapcy(screenWidth/2 , screenHeight /2, 0.7);
	
		
		//animasi obstacle
		this.anims.create({
			key: 'glow',
			frames: [{key: "obstacle", frame: 0},
					{key: "obstacle", frame: 1},
					{key: "obstacle", frame: 2},
					{key: "obstacle", frame: 3},
					{key: "obstacle", frame: 4},
					{key: "obstacle", frame: 5},
					{key: "obstacle", frame: 6},
					{key: "obstacle", frame: 7},
					{key: "obstacle", frame: 7},
					{key: "obstacle", frame: 7},
					{key: "obstacle", frame: 7}],
			frameRate: 10,
			repeat: -1,
			repeatDelay: 500,
			yoyo:true
		});
		
		this.tailParticle = this.add.particles('tail');
		
		this.impactParticle = this.add.particles('impact');
		this.impactParticle.createEmitter({
			speed: 0,
			scaleX : {start : 0 , end : 3},
			scaleY : {start : 0 , end : 3},
			alpha: {start : 1 , end : 0},
			blendMode: 'ADD',
			lifespan: 1000,
			quantity: 3,
			on: false
		});
		
		this.warpParticle = this.add.particles('wrap');
		this.warpParticle.createEmitter({
			speed: 0,
			scaleX : {start : 3 , end : 0},
			scaleY : {start : 3 , end : 0},
			alpha: {start : 1 , end : 0},
			blendMode: 'ADD',
			lifespan: 1000,
			quantity: 3,
			on: false
		});
		
		
		console.log(this.capcy);

		Phaser.Actions.RandomRectangle(this.capcy, this.cameras.main);
		
		this.input.mouse.capture = true;
		
		let started = new Date().getTime();
		requestNewCredential((cred)=>{
			let ended = new Date().getTime();
			this.credDelay = ended - started;
			console.log("Delay : "+this.credDelay)
			
			this.credential = cred;
			this.tweens.add({
				targets: this.loading,
				ease: "Power1",
				duration: 500,
				y: screenHeight /2 - screenHeight
			})
			this.showHintStage();
			this.isStarted = true;
			
			this.timerFinal = this.addObsTimeEvery;
			console.log("Credential : "+cred);
			
		});
		
		
	},
	secLooper : 0,
	isDownSound : false,
	downIdx : -1,
	update:function(time, delta){
		
		if(this.isDownSound){
			if(this.downIdx == 0){
					let v = Math.max(this.bgSound1.volume - 0.01 , 0)
					this.bgSound1.setVolume(v);
					if(v == 0){
						this.bgSound1.stop();
						this.isDownSound =false;
					}
			}else if(this.downIdx == 1){
					let v = Math.max(this.bgSound2.volume - 0.01 , 0)
					this.bgSound2.setVolume(v);
					if(v == 0){
						this.bgSound2.stop();
						this.isDownSound =false;
					}
			}else if(this.downIdx == 2){
					let v = Math.max(this.bgSound3.volume - 0.01 , 0)
					this.bgSound3.setVolume(v);
					if(v == 0){
						this.isDownSound =false;
						this.bgSound3.stop();
					}
			}
				
			
		}
		
		if(this.isStarted == false){
			return;
		}
		
		//if(this.timeCounting){
		//	this.timePassed += delta;
		//}
		
		this.userInputHandler();
		this.obstaclesDetectCollision();
		
		
		this.secLooper += delta;
		if(this.secLooper > 300){
			this.loopEverySecond()
			this.secLooper = 0;
			this.waveText.text = "Final wave start in : "+Math.floor( this.timerFinal/1000 );
		}
		
		if(this.isFinished){
			return;
		}
		
		this.counterObs -= delta;
		this.timerFinal = Math.max( 0 , Math.min(this.timerFinal , this.counterObs) );
		
		if(this.counterObs < 0){
			
			this.timerFinal = 0;
			this.waveText.text = "Final wave start in : "+Math.floor( this.timerFinal/1000 );
			this.counterObs = this.addObsTimeEvery  + this.timeCooldown;
			this.secLooper = -this.timeCooldown
			
			this.time.addEvent({
				delay: this.timeCooldown,                // ms
				callback: ()=>{
								this.stage++;
								
								this.stageHeadingText.text = "Stage "+this.stage;
								this.showHintStage();
								
								this.stageText.text = "Stage : "+this.stage;
								this.timerFinal = this.addObsTimeEvery;
								
								this.xObs -= this.fWave;
								this.yObs -= this.fWave;
								
								this.fWave++;
								
								},
				loop: false
			});
			
			this.time.addEvent({
				delay: this.timeCooldown*0.6,                // ms
				callback: ()=>{this.stageClear()},
				loop: false
			});
			
			this.xObs += this.fWave;
			this.yObs += this.fWave;
			let loopSummon = this.xObs + this.yObs;
			for(let i = 0; i < loopSummon; i++){
				this.obstacles.push(this.createObstacle());
			}
			this.xObs++;
			this.yObs++;
			
		}
	},
	stageClear:function(){
		if(!this.isFinished){
			this.clearSound.play();
			this.stageHeadingText.text = "Stage Clear!";
			this.showHintStage();
			
			//if(this.stage % 4== 0){
				
			let idx = (this.stage%3)
			
			switch(idx){
				case 0:
					//this.soundGame3.stop();
					this.isDownSound = true;
					this.downIdx = 2;
					this.bgSound1.setVolume(0.5);
					this.bgSound1.play();
				break;
				case 1:
					this.isDownSound = true;
					this.downIdx = 0;
					this.bgSound2.setVolume(0.5);
					this.bgSound2.play();
				break;
				case 2:
					this.isDownSound = true;
					this.downIdx = 1;
					this.bgSound3.setVolume(0.5);
					this.bgSound3.play();
				break;
				
			}
				
				
			//}
			
		}
	},
	showHintStage:function(){
		
		this.stageHeadingText.alpha = 0;
		
		this.tweens.add({
			targets: this.stageHeadingText,
			ease: "Quad.easeInOut",
			duration: 1500,
			alpha : 1,
			loop: 0,
			yoyo: true
		})
	},
	isFinished : false,
	obstaclesDetectCollision:function(){
		if(this.isFinished){
			return;
		}
		for (let i = this.obstacles.length - 1; i >= 0; i--) 
		{
			
			dist = distance(this.capcy , this.obstacles[i])
			
			if(dist < 50+(50 * this.obstacles[i].scaleSize && !this.isFinished)){
				console.log("Game Finished " , this.obstacles[i].scaleSize , this.isFinished);
				if(this.isFinished == false){
					this.gameFinished();
					
				}
			}
		}

	},
	gameFinished:function(){
		this.isFinished = true;
		this.capcyDie();
		this.cameras.main.shake(500);
		
		
		
		//Fade out
		for (let i = this.obstacles.length - 1; i >= 0; i--) 
		{
			this.tweens.add({
				targets: this.obstacles[i].obj,
				ease: "Power1",
				duration: 1500,
				alpha: 0
			})
			
			this.obstacles[i].tail.stop();
		}
		
		/*
		
		*/
		showLoading();
		
		let started = new Date().getTime();
		isInHighScore(this.score , this.credential,(result)=>{
			let ended = new Date().getTime();
			let milliseconds = ended - started;
			this.timePassed = (new Date().getTime() - this.timePassed) -(milliseconds*0.6) -(this.credDelay*0.6) ;
			
			
			
			if(result){
				
				console.log("New High Score");
				//this.closeGameLayer(()=>{this.destroyAll();this.moveToResult();});
				let data = {creds : this.credential , userName : '' , score : this.score , timePassed:this.timePassed , stage: this.stage , timePerStage : (this.addObsTimeEvery + this.timeCooldown) , timeOffset : milliseconds}
				setNewHighScore(data , (result)=>{
					console.log( "delay : "+ milliseconds+" res : "+ result.timeDiff+" stg : "+((data.timePerStage*data.stage)+milliseconds)+" total :"+this.timePassed);
					if(result.result){
						showSuccess(result.message)
					}else{
						showFailed(result.message)
					}
				
					this.closeGameLayer(()=>{this.destroyAll();this.moveToResult();})
					
				})
				/*
				saveScore( data , (result2)=>{
					console.log(result2);
					this.closeGameLayer();
				})
				*/
			}else{
				hideModal()
				console.log("Not a High Score");
				this.closeGameLayer(()=>{this.destroyAll();this.moveToResult();});
			}
		})
		
		
	},
	closeGameLayer:function(callbackFunction){
		//tutup layer
		this.tweens.add({
			targets: this.loading,
			ease: "Power1",
			duration: 500,
			y: screenHeight /2,
			delay: 2000
		})
		
		// ganti scene
		this.time.addEvent({
			delay: 3000,                // ms
			callback: callbackFunction,
			loop: false
		});
	},
	userInputHandler:function(){
		
		if(this.isFinished){
			return;
		}
		
		if(this.input.activePointer.justDown){
			//console.log(this.input.activePointer.identifier);
			vecA = this.input.activePointer.position;
			vecB = {x: this.capcy.x , y : this.capcy.y};
			
			dirr = direction(vecA , vecB);
			
			power = 300*clamp( 0 , 1 , inverseLerp(350 , 30 , dirr.magnitude) );
			if(power > 0){
				this.closeEyeCapcy();
				
				//this.capcyDie();
				
				if(this.input.activePointer.rightButtonDown()){ // kalo klik kanan tarik
				
					let temp = this.sound.add('soundWarp' , {volume : 0.5});
					temp.loop = true;
					temp.once('looped', function(music){music.stop();music.destroy();});
					temp.play();
				
					
					this.cameras.main.shake(80);
					this.warpParticle.emitParticleAt(vecA.x , vecA.y);
				
					nextX = dirr.direction.x * power + this.capcy.body.velocity.x
					nextY = dirr.direction.y * power + this.capcy.body.velocity.y
				
					this.capcy.body.setVelocity(nextX , nextY);
				}else{ //kalo klik kiri dorong
					//this.impactSound.play();
					
					let temp = this.sound.add('soundImpact' , {volume : 0.5});
					temp.loop = true;
					temp.once('looped', function(music){music.stop();music.destroy();});
					temp.play();
					
					this.cameras.main.shake(80);
					//tempEmit.setPosition(vecA.x , vecA.y);
					this.impactParticle.emitParticleAt(vecA.x , vecA.y);
					
					nextX = -dirr.direction.x * power + this.capcy.body.velocity.x
					nextY = -dirr.direction.y * power + this.capcy.body.velocity.y
				
					this.capcy.body.setVelocity(nextX , nextY);
				}
			}
			
			
			
		}else {
			
			out = this.isOutOfMap(this.capcy.x ,this.capcy.y , this.capcy.body.halfWidth,this.capcy.body.halfHeight , this.capcy.body.velocity.x , this.capcy.body.velocity.y , 0);
			if(out != 0){
				this.closeEyeCapcy();
				nextX = this.capcy.body.velocity.x * 0.9
				nextY = this.capcy.body.velocity.y * 0.9
				if(out == 1){
					nextX = -nextX
				}else{
					nextY = -nextY
				}
				
				this.capcy.body.setVelocity(nextX , nextY);
			}else {

				dirr = normalize(this.capcy.body.velocity)
				if(dirr.magnitude!=0){
					//console.log(dirr);
					nextX = this.capcy.body.velocity.x - (dirr.direction.x * this.capcy.decreaseVelocity);
					nextY = this.capcy.body.velocity.y - (dirr.direction.y * this.capcy.decreaseVelocity);
					
					//console.log(nextX , nextY)
					this.capcy.body.setVelocity(nextX , nextY);
				}
				
				if (this.capcy.body.velocity.x < 0)
				{
					this.capcy.rotation -= (0.02 * inverseLerp(0 , -200 , this.capcy.body.velocity.x));
				}
				else if(this.capcy.body.velocity.x > 0)
				{
					this.capcy.rotation += (0.02 * inverseLerp(0 , 200 , this.capcy.body.velocity.x));;
				}
			}
			
		}
		//vecC = {x: this.capcy.x , y : this.capcy.y};
		//console.log(vecC);
	},
	capcyDie:function(){
		
		this.dieSound.play();
		
		let x = this.capcy.x;
		let y = this.capcy.y;
		let velX = this.capcy.body.velocity.x
		let velY = this.capcy.body.velocity.y
		let rotation = this.capcy.rotation;
		//if(rotation < 0){
			
		//}
		
		let vec = angleToVector(rotation)
		console.log(vec , rotation);
		
		this.createBodyPart(x-vec.x*35 , y-vec.y*35  , velX ,velY, rotation, 'dieP1');
		this.createBodyPart(x , y ,velX ,velY, rotation, 'dieP2');
		this.createBodyPart(x+vec.x*35 , y+vec.y*35 ,velX ,velY, rotation, 'dieP3');
		
		this.capcy.destroy();
		
	},
	createBodyPart:function(x , y  , velX,velY , rotation, name){
		let capcy = this.add.container(x , y);
		let body = this.add.sprite(0,0, name);
		body.setScale(0.4);
		
		capcy.add(body);
		this.physics.world.enable(capcy);
		capcy.rotation = rotation
		capcy.body.setVelocity(velX + ((Math.random()-0.5)*100) , velY + ((Math.random()-0.5)*100))
		return capcy;
	},
	loopEverySecond:function(){
		
		if(this.isFinished){
			return;
		}
		
		
		
		
		
		//console.log("Loop Here");
		if(this.xObs + this.yObs > 0){
			this.obstacles.push(this.createObstacle());
		}
		
		for (let i = this.obstacles.length - 1; i >= 0; i--) 
		{
			if(this.isOutOfMap(this.obstacles[i].x ,this.obstacles[i].y , 10 , 10 , this.obstacles[i].body.velocity.x , this.obstacles[i].body.velocity.y , 0)){
				//console.log("obstacle Destroyed : ",Math.abs( this.obstacles[i].body.velocity.x ));
				if(Math.abs( this.obstacles[i].body.velocity.x )> 0){
					this.xObs++;
				}else{
					this.yObs++;
				}
				//console.log(this.obstacles[i].tail);
				this.obstacles[i].tail.stop()
				this.obstacles[i].destroy();
				this.obstacles.splice(i, 1);
				if(!this.isFinished){
					this.score++;
					this.scoreText.text = "Score : "+this.score;
				}
			}
		}
	},
	closeEyeCapcy:function(){
		
		if(this.capcy.eye1._visible == true){
			this.capcy.blink.play();
		
		}
		
		this.capcy.eye1.setVisible(false);
		this.capcy.eye2.setVisible(true);
		
		
		if(this.capcy.timer != null){
			this.capcy.timer.remove();
		}
		this.capcy.timer = this.time.addEvent({
			delay: 700,                // ms
			callback: ()=>{
							this.capcy.eye1.setVisible(true);
							this.capcy.eye2.setVisible(false);
							this.capcy.timer = null;
							this.capcy.blink.play();
		
							},
			loop: false
		});
	},
	createCapcy:function(x , y, friction){
		
		let capcy = this.add.container(x , y);
		
		let body = this.add.sprite(0,0, 'body');
		body.setScale(0.4);
		
		let eye1 = this.add.sprite(0,5, 'eye1');
		eye1.setOrigin(0.5, 0.85);;
		eye1.setScale(0.4);
		this.tweens.add({
            targets: eye1,
            ease: "Quad.easeInOut",
            duration: 300,
            scaleY: 0.05,
			loop: -1,
			yoyo: true,
			loopDelay: 2000
        })
		
		let eye2 = this.add.sprite(0,0, 'eye2');
		eye2.setScale(0.4);
		eye2.setVisible(false);
		
		capcy.add(body);
		capcy.add(eye1);
		capcy.add(eye2);
		
		capcy.eye1 = eye1;
		capcy.eye2 = eye2;
		
		capcy.blink = this.sound.add('blink');
		capcy.blink.setVolume(0.5);
		
		this.physics.world.enable(capcy);
		
		capcy.decreaseVelocity = friction;
		capcy.body.setMaxVelocity(1000 , 1000);
		
		
		return capcy
		
	},
	xObs : 10,
	yObs : 10,
	createObstacle:function(){
		x = 0;
		y = 0;
		offset = 100;
		
		velocity = {x:0 , y:0}
		
		if(this.xObs  >= this.yObs && this.xObs > 0){
			this.xObs--;
			fromLeft = Math.round( Math.random() < 0.5 )
			if(fromLeft){
				x = 0 - offset
				velocity.x = 150 + (Math.random()*200)
			}else{
				x = screenWidth + offset;
				velocity.x = -150 + (Math.random()*-200)
			}
			y = Math.random() * screenHeight;
		}else if(this.yObs > this.xObs && this.yObs > 0){
			this.yObs--;
			fromUp = Math.round( Math.random() < 0.5 )
			if(fromUp){
				y = 0 - offset
				velocity.y = 150 + (Math.random()*200)
			}else{
				y = screenHeight + offset
				velocity.y = -150 + (Math.random()*-200)
			}
			x = Math.random() * screenWidth;
		}
		
		
		let scaleSize = 0.5 + Math.random()*0.4;
		
		let obs = this.add.container(x , y);
		
		let emitter = this.tailParticle.createEmitter({
			speed: 0,
			scaleX : scaleSize,
			scaleY : scaleSize,
			alpha: {start : 1 , end : 0},
			blendMode: 'ADD'
		});
		
		
		let body = this.add.sprite(0,0, 'obstacle');
		body.setScale(scaleSize );
		//console.log(this.anims.generateFrameNumbers('obstacle', { start: 0, end: 7 }));
		
		
		
		body.anims.play('glow', true);
		
		
		
		
		
		emitter.startFollow(obs);
		
		obs.add(body);
		obs.obj = body;
		//obs.add();
		obs.tail = emitter;
		obs.scaleSize = scaleSize
		this.physics.world.enable(obs);
		obs.body.setVelocity(velocity.x , velocity. y);
		
		return obs
	},
	isOutOfMap:function(x , y , halfWidth , halfHeight, velX , velY , offset){
		if(x-halfWidth < 0 -offset|| x + halfWidth > screenWidth+offset){
			return velX/Math.abs(velX) == (x-1-halfWidth)/Math.abs(x-1-halfWidth) ?  1 : 0;
		}else if(y - halfHeight < 0-offset || y + halfHeight > screenHeight+offset){
			return velY/Math.abs(velY) == (y-1-halfHeight)/Math.abs(y-1-halfHeight) ?  2 : 0;
		}
		
		return 0;
		
	},
	destroyAll:function(){
		for (let i = this.obstacles.length - 1; i >= 0; i--) 
		{
			this.obstacles[i].tail.stop()
			this.obstacles[i].destroy();
			this.obstacles.splice(i, 1);
		}
		this.bgSound1.stop();
		this.bgSound2.stop();
		this.bgSound3.stop();
		this.tailParticle.destroy();
		
	},
	moveToResult:function(){
		
		this.scene.start('result', {score : this.score , stage : this.stage});
		
	}
	
})




