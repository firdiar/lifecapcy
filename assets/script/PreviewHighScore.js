PreviewHighScore= new Phaser.Class({
    Extends: Phaser.Scene,

	initialize: function Boot ()
    {
        // Pembuatan scene baru
        Phaser.Scene.call(this, { key: 'previewHighScore' });
    },
    create: function() {
		
		if(this.clickSound==null){
			this.clickSound = this.sound.add('click');
			this.clickSound.setVolume(0.5);
		}
		
		this.bg = this.add.image(screenWidth/2 , screenHeight /2, 'highScore');
		
		this.loading = this.add.image(screenWidth/2 , screenHeight /2, 'loading');
		this.loading.setDepth(10);
		
		scores = []
		
		scores.push(playerScore(155 , 60 , this))
		
		scores.push(playerScore(245 , 35, this))
		scores.push(playerScore(310 , 35, this))
		
		for (let i = 4 ; i <= 10; i++){
			scores.push(playerScore(365 + (40*(i-4)) , 20, this))
		}
		
		getHighScoreName((result)=>{
			let sorted = this.loadData(result);
			
			for(let i= 0 ; i< sorted.length; i++){
				scores[i].pname.text = sorted[i][0]
				scores[i].score.text = sorted[i][1]
			}
			
			this.tweens.add({
				targets: this.loading,
				ease: "Power1",
				duration: 500,
				y: screenHeight /2 - screenHeight
			})
			
		})
		
		
		this.btnStart = this.add.sprite(screenWidth * 0.11 , screenHeight *0.94, 'backBtn').setScale(0.5)
		 .setInteractive()
		 .on('pointerdown', () => {this.BackToMain(); this.clickSound.play();} )
		 .on('pointerover', () => { 
				this.btnStart.setFrame(1);  
				} )
		 .on('pointerout', () =>  { this.btnStart.setFrame(0);});
		
    },
	loadData:function(highScore){
	
		
		//sort to get highest score
		var sortable = [];
		for (var key in highScore) {
			sortable.push([key, highScore[key]]);
		}


		sortable.sort(function(a, b) {
			return b[1] - a[1];
		});
		
		return sortable;
		
	},
	BackToMain:function(){
		this.tweens.add({
            targets: this.loading,
            ease: "Power1",
			onComplete : ()=> {this.scene.start('mainmenu', {test : 'empty'});},
            duration: 500,
            y: screenHeight/2
        })
	}
})

function playerScore(y , size , scene){
	pname =  scene.add.text(250, y, 'Computer', { fontFamily: 'Impact' , font: '"Press Start 2P"' }).setFontSize(size);
	score =  scene.add.text(1000, y, '0', { fontFamily: 'Impact' , font: '"Press Start 2P"' }).setFontSize(size);
	
	return {pname:pname , score:score}
}


