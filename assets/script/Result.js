Result = new Phaser.Class({
    Extends: Phaser.Scene,

	initialize:
	function Result()
    {
        // Pembuatan scene baru
        Phaser.Scene.call(this, { key: 'result' });
    },
	scoreCounter : 0,
	scoringFinished : false,
	targetScore : 0,
	deltaTime : 0,
	create : function(config){
		
		var bestScore = localStorage.getItem('highScore') || 0;
		
		if(this.bgSound==null){
			this.bgSound = this.sound.add('soundGameOver');
			this.bgSound.loop = true;
			this.bgSound.setVolume(0.35);
		}
		if(this.clickSound==null){
			this.clickSound = this.sound.add('click');
			this.clickSound.setVolume(0.5);
		}
		
		this.bgSound.play();

		this.scoreCounter = 0;
		this.scoringFinished = false;
		this.targetScore = 0;
		this.deltaTime = 0;
		
		this.loading = this.add.image(screenWidth/2 , screenHeight /2, 'loading');
		this.loading.setDepth(10);
		this.tweens.add({
            targets: this.loading,
            ease: "Power1",
            duration: 500,
            y: screenHeight /2 - screenHeight
        })
		
		if(config.score > bestScore){
			bestScore = config.score
			localStorage.setItem('highScore' , config.score)
		}
		
		this.targetScore = config.score;
		
		this.add.image(screenWidth/2 , screenHeight /2, 'bgMain');
		
		let body = this.add.image(200 , screenHeight /2-100, 'bodyHappy').setScale(0.8);
		
		let temp = this.add.text(460, 150, 'Game Result', { fontFamily: 'Impact' , font: '"Press Start 2P"' });
		temp.setFontSize(70);
		
		this.stage= this.add.text(500, 240, 'Stage : '+config.stage, { fontFamily: 'Impact' , font: '"Press Start 2P"' });
		this.stage.setFontSize(50);
		
		this.score = this.add.text(500, 330, 'Score : 0', { fontFamily: 'Impact' , font: '"Press Start 2P"' });
		this.score.setFontSize(50);
		
		this.bscore = this.add.text(500, 420, 'High Score : '+bestScore, { fontFamily: 'Impact' , font: '"Press Start 2P"' });
		this.bscore.setFontSize(50);
		
		this.tweens.add({
            targets: body,
            ease: "Quad.easeInOut",
            duration: 5000,
            y: 400,
			loop: -1,
			yoyo: true,
			loopDelay: 1000
        })
		
		this.btnMain = this.add.sprite(400, 550, 'backBtn')
		 .setInteractive()
		 .on('pointerdown', () =>{
			 console.log("Back to main!");
			 this.clickSound.play();
			 this.bgSound.stop();
			 this.gotoMain();
			 } )
		 .on('pointerover', () => { 
				this.btnMain.setFrame(1); } )
		 .on('pointerout', () =>  { this.btnMain.setFrame(0);} );
		 this.btnMain.setScale(0.8);
		 this.btnMain.setOrigin(0,0.5);
		 
		 
		 
		 this.btnPlay = this.add.sprite(720, 550, 'playAgainBtn')
		 .setInteractive()
		 .on('pointerdown', () =>{
			 console.log("Back to main!");
			 this.clickSound.play();
			 this.bgSound.stop();
			 this.playAgain();
			 } )
		 .on('pointerover', () => { 
				this.btnPlay.setFrame(1); } )
		 .on('pointerout', () =>  { this.btnPlay.setFrame(0);} );
		 this.btnPlay.setScale(0.8);
		 this.btnPlay.setOrigin(0,0.5);
		
		console.log(config);
	},
	
	gotoMain:function(){
		this.tweens.add({
            targets: this.loading,
            ease: "Power1",
			onComplete : ()=> {this.scene.start('mainmenu', {test : 'empty'});},
            duration: 500,
            y: screenHeight /2
        })
	},
	
	playAgain:function(){
		
		this.tweens.add({
            targets: this.loading,
            ease: "Power1",
			onComplete : ()=> {this.scene.start('gamescene', {test : 'empty'});},
            duration: 500,
            y: screenHeight /2
        })
	},
	
	update: function(time , delta){
		if(this.scoringFinished == false){
			this.deltaTime += delta
			let val = clamp( 0 , 1 , inverseLerp(0 , 2000 , this.deltaTime));
			this.scoreCounter = this.targetScore * val;
			this.score.setText("Score : "+ (Math.floor(this.scoreCounter*100)/100));
			//console.log(this.deltaTime , val);
			if(val == 1){
				this.scoringFinished = true;
			}
		}
	}
});