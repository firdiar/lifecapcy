Boot = new Phaser.Class({
    Extends: Phaser.Scene,

	initialize:
	function Boot ()
    {
        // Pembuatan scene baru
        Phaser.Scene.call(this, { key: 'boot' });
    },
	init: function(config){
        console.log('[GAME] init', config);
		this.input.maxPointers = 2;
		
		//this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		//this.scale.pageAlignHorizontally = true;
		//this.scale.pageAlignVertically = true;
		//this.stage.disableVisibilityChange = true;

    },
    preload: function() {
        console.log('[GAME] preload');
		this.loading = this.add.image(screenWidth/2 , screenHeight /2, 'loading');
		
		this.load.audio('click', "assets/audio/clickButton.mp3");
		this.load.audio('blink', "assets/audio/blink.mp3");
		
		
		
		this.load.audio('soundMain', "assets/audio/'Embrace' by Sappheiros Chill Instrumental [Non Copyrighted Music].mp3");
		this.load.image('bgMain', 'assets/image/mainmenuBG.png');
		this.load.image('title', 'assets/image/title.png');
		this.load.image('desc', 'assets/image/desc.png');
		this.load.image('gameIcon', 'assets/image/gameIcon.png');
		this.load.spritesheet( 'startBtn', 'assets/image/buttonStart.png' , {frameWidth:500,frameHeight:200});
		this.load.image('eye1', 'assets/image/eye1.png');
		this.load.image('eye2', 'assets/image/eye2.png');
		
		this.load.spritesheet( 'guideBtn', 'assets/image/buttonGuide.png' , {frameWidth:500,frameHeight:200});
		this.load.image('guide', 'assets/image/Guide.png');
		
		this.load.spritesheet( 'creditBtn', 'assets/image/buttonCredits.png' , {frameWidth:500,frameHeight:200});
		this.load.image('credit', 'assets/image/Credits.png');
		
		
		
		this.load.spritesheet( 'highScoreBtn', 'assets/image/buttonHighScore.png' , {frameWidth:500,frameHeight:200});
		this.load.image('highScore', 'assets/image/Highscore.png');
		
		
		this.load.audio('soundGame1', "assets/audio/'Art Of Silence' by Uniq Ambient Music (No Copyright) (mp3cut.net).mp3");
		this.load.audio('soundGame2', "assets/audio/bensound-memories (mp3cut.net).mp3");
		this.load.audio('soundGame3', "assets/audio/bensound-summer (mp3cut.net).mp3");
		
		this.load.audio('soundClear', "assets/audio/sClear.mp3");
		this.load.audio('soundDie', "assets/audio/dead.mp3");
		this.load.audio('soundWarp', "assets/audio/Warp.mp3");
		this.load.audio('soundImpact', "assets/audio/impact.mp3");
		
		this.load.image('bg', 'assets/image/background.png');
		this.load.spritesheet( 'playAgainBtn', 'assets/image/buttonPlayAgain.png' , {frameWidth:500,frameHeight:200});
		this.load.spritesheet( 'obstacle', 'assets/image/GlowParticleSpriteSheet.png' , {frameWidth:90,frameHeight:90});
		this.load.image('tail', 'assets/image/tail.png');
		this.load.image('body', 'assets/image/body.png');
		this.load.image('dieP1', 'assets/image/die1.png');
		this.load.image('dieP2', 'assets/image/die2.png');
		this.load.image('dieP3', 'assets/image/die3.png');
		this.load.image('impact', 'assets/image/impact.png');
		this.load.image('wrap', 'assets/image/wrap.png');
		
		this.load.audio('soundGameOver', "assets/audio/Gameover.mp3");
		
		this.load.spritesheet( 'backBtn', 'assets/image/buttonBack.png' , {frameWidth:500,frameHeight:200});
		this.load.image('bodyHappy', 'assets/image/happyBody.png');
		
		
    },
    create: function() {
        console.log('[GAME] create');
		wakingUpAPi(()=>{
			this.scene.start('mainmenu', {test : 'empty'});
		});
		
    },
    update: function(){
        console.log('[GAME boot] update');
    }
})


