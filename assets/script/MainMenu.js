MainMenu = new Phaser.Class({
    Extends: Phaser.Scene,

	initialize:
	function MainMenu ()
    {
        // Pembuatan scene baru
        Phaser.Scene.call(this, { key: 'mainmenu' });
    },
	loading : null,
    create: function()  {
		console.log('[GAME menu] create');
		
		this.loading = this.add.image(screenWidth/2 , screenHeight /2, 'loading');
		this.loading.setDepth(10);
		this.tweens.add({
            targets: this.loading,
            ease: "Power1",
            duration: 500,
            y: screenHeight /2 - screenHeight
        })
		
		if(this.bgSound==null){
			this.bgSound = this.sound.add('soundMain');
			this.bgSound.loop = true;
			this.bgSound.setVolume(0.35);
		}
		if(this.clickSound==null){
			this.clickSound = this.sound.add('click');
			this.clickSound.setVolume(0.5);
		}
		if(this.blinkSound==null){
			this.blinkSound = this.sound.add('blink');
			this.blinkSound.setVolume(0.5);
		}
		
		
		
		if(!this.bgSound.isPlaying){
			this.bgSound.play();
		}
        
		//console.log(this.bgSound);
		
		//add background
        this.add.image(screenWidth/2 , screenHeight /2, 'bgMain');
		
		//add title text game
		let temp = this.add.image(screenWidth/2 , screenHeight /6, 'title');
		temp.setScale(0.8);
		
		// add game icon
		let container = this.add.container(screenWidth * 0.32 , screenHeight *0.60);
        container.add( this.add.sprite(0,0, 'gameIcon') )
		
		this.eye = this.add.sprite(-25 , -70, 'eye1')
		this.eye.setOrigin(0.5, 0.85);
		this.eye.setScale(0.8);
		//console.log(this.eye);
		this.tweens.add({
            targets: this.eye,
            ease: "Quad.easeInOut",
            duration: 350,
            scaleY: 0.2,
			loop: -1,
			yoyo: true,
			loopDelay: 2000
        })
		container.add(this.eye)
		
		this.eye2 = this.add.sprite(-25 , -80, 'eye2')
		this.eye2.setVisible(false);
		this.eye2.setScale(0.8);
		container.add(this.eye2)
		
		//console.log(container);
		
		this.tweens.add({
            targets: container,
            ease: "Quad.easeInOut",
            duration: 5000,
            y: 480,
			loop: -1,
			yoyo: true,
			loopDelay: 2000
        })
		
		
		//add button desc
		temp = this.add.image(screenWidth * 0.8 , screenHeight *0.48, 'desc');
		
		//add button startGame
		//temp = this.add.button(screenWidth * 0.8 , screenHeight *0.7, 'startBtn' , 0 , 1, 0);
		
		this.btnStart = this.add.sprite(screenWidth * 0.8 , screenHeight *0.6, 'startBtn')
		 .setInteractive()
		 .on('pointerdown', () => {this.gameStart(); this.clickSound.play();} )
		 .on('pointerover', () => { 
				this.btnStart.setFrame(1);  
				this.closeEye(true);
				} )
		 .on('pointerout', () =>  { 
				this.btnStart.setFrame(0);
				//this.closeEye(false);
				this.openEye = true;
				} );
				
				
		this.btnHS = this.add.sprite(screenWidth * 0.8 , screenHeight *0.72, 'highScoreBtn').setScale(0.6)
		 .setInteractive()
		 .on('pointerdown', () => { this.highScore();this.clickSound.play();} )
		 .on('pointerover', () => { 
				this.btnHS.setFrame(1);  
				this.closeEye(true);
				
				} )
		 .on('pointerout', () =>  { 
				this.btnHS.setFrame(0);
				//this.closeEye(false);
				this.openEye = true;
				} );
				
				
		
		
		 
		 this.btnGuide = this.add.sprite(screenWidth * 0.8 , screenHeight *0.81, 'guideBtn').setScale(0.6)
		 .setInteractive()
		 .on('pointerdown', () => {this.guide();this.clickSound.play();} )
		 .on('pointerover', () => { 
				this.btnGuide.setFrame(1);  
				this.closeEye(true);
				} )
		 .on('pointerout', () =>  { 
				this.btnGuide.setFrame(0);
				//this.closeEye(false);
				this.openEye = true;
				} );
				
		 this.btnCredits = this.add.sprite(screenWidth * 0.8 , screenHeight *0.9, 'creditBtn').setScale(0.6)
		 .setInteractive()
		 .on('pointerdown', () => { this.credits();this.clickSound.play();} )
		 .on('pointerover', () => { 
				this.btnCredits.setFrame(1);  
				this.closeEye(true);
				
				} )
		 .on('pointerout', () =>  { 
				this.btnCredits.setFrame(0);
				//this.closeEye(false);
				this.openEye = true;
				} );
				
		
		 
		 //console.log(this.btnStart);
		
    },
	openEye : false,
	count : 0,
	update:function(time , delta){
		
		if(this.openEye){
			this.count += delta;
			if(this.count > 100){
				this.openEye = false;
				this.count = 0;
				this.closeEye(false);
			}
			
		}
		
	},
	closeEye:function(isClose){
		
		//console.log(this.eye._visible);
		if(this.eye._visible == isClose){
			this.blinkSound.play();
		}
		if(isClose){
			this.openEye = false;
			this.count = 0;
		}
		this.eye.setVisible(!isClose);
		this.eye2.setVisible(isClose);
		
	},
	highScore:function(){
		this.tweens.add({
            targets: this.loading,
            ease: "Power1",
			onComplete : ()=> {this.scene.start('previewHighScore', {test : 'empty'});},
            duration: 500,
            y: screenHeight /2
        })
	},
	credits:function(){
		this.tweens.add({
            targets: this.loading,
            ease: "Power1",
			onComplete : ()=> {this.scene.start('credits', {test : 'empty'});},
            duration: 500,
            y: screenHeight /2
        })
	},
	guide:function(){
		this.tweens.add({
            targets: this.loading,
            ease: "Power1",
			onComplete : ()=> {this.scene.start('guide', {test : 'empty'});},
            duration: 500,
            y: screenHeight /2
        })
	},
	gameStart(){
		console.log("Clicked");
		this.bgSound.stop();
		this.tweens.add({
            targets: this.loading,
            ease: "Power1",
			onComplete : ()=> {this.scene.start('gamescene', {test : 'empty'});},
            duration: 500,
            y: screenHeight /2
        })
		
	}
})