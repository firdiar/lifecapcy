var gameRef = null;

var screenWidth = 1280;
var screenHeight = 720;

var hostName = "https://lifecapcy.herokuapp.com"

function distance(vectorA , vectorB){
	return Math.sqrt(Math.pow(vectorA.x - vectorB.x , 2) +Math.pow(vectorA.y - vectorB.y , 2));
}


//#Region API

function ping(callback) {

  var started = new Date().getTime();

  var http = new XMLHttpRequest();

  http.open("GET", hostName, /*async*/true);
  http.onreadystatechange = function() {
    if (http.readyState == 4) {
      var ended = new Date().getTime();

      var milliseconds = ended - started;

      if (callback != null) {
        callback(milliseconds);
      }
    }
  };
  try {
    http.send(null);
  } catch(exception) {
    // this is expected
  }

}

function wakingUpAPi(callback){
	$.get(hostName+"/" , (json)=>{
		console.log(json)
		callback();
	})
}

function getHighScore(callback){
	$.get(hostName+"/highScore" , (json)=>{
		callback(json)
	})
}


function getHighScoreName(callback){
	$.get(hostName+"/highScoreName" , (json)=>{
		callback(json)
	})
}

function isInHighScore(score, credentials , callback){
	console.log({score : score , creds : credentials})
	$.post(hostName+"/isInHighScore" , {score : score , creds : credentials} , (json)=>{
		callback(json);
	})
}

function requestNewCredential(callback){
	
	$.get(hostName+"/playNewGame" , (json)=>{
		
		callback(json);
	})
}

function saveScore( data , callback){
	//{creds : '' , userName : '' , score : 0 , timePassed:0 , stage: 5 , timePerStage : 0}
	$.post(hostName+"/newHighScore" , data , (json)=>{
		console.log(json);
		callback(json);
	})
}
//#End Region API

function resizeApp()
{
	// Width-height-ratio of game resolution
	let game_ratio = 1280 / 720;
	let div = document.getElementById('phaser-app');
	console.log(window.innerWidth , window.innerHeight);
	if(window.innerWidth / window.innerHeight > game_ratio){
		// Make div full height of browser and keep the ratio of game resolution
		div.style.width = (window.innerHeight * game_ratio) + 'px';
		div.style.height = window.innerHeight + 'px';
	}else{
		// Make div full height of browser and keep the ratio of game resolution
		div.style.width = (window.innerWidth ) + 'px';
		div.style.height = (window.innerWidth / game_ratio)+ 'px';
	}
	let canvas = document.getElementsByTagName('canvas')[0];
	let dpi_w = (parseInt(div.style.width));
	let dpi_h = (parseInt(div.style.height));		
	
	canvas.style.width = dpi_w + 'px';
	canvas.style.height = dpi_h + 'px';
}

function direction(vectorA , vectorB){
	newVec = {x : vectorA.x - vectorB.x , y: vectorA.y - vectorB.y};
	return normalize(newVec);
	/*
	length = Math.sqrt(Math.pow(newVec.x , 2) + Math.pow(newVec.y , 2));
	newVec .x = newVec .x /  length;
	newVec .y = newVec .y /  length;
	
	return  {direction : newVec , magnitude : length};*/
}

function normalize(vector){
	vectorA = {x:vector.x  , y:vector.y}
	length = Math.sqrt(Math.pow(vectorA.x , 2) + Math.pow(vectorA.y , 2));
	if(length == 0){
		return {direction : {x:0 , y:0} , magnitude : length};
	}
	vectorA.x = vectorA.x /  length;
	vectorA.y = vectorA.y /  length;
	
	return  {direction : vectorA , magnitude : length};
}

function angleToVector(angle){
	
	return {x:Math.cos(angle), y:Math.sin(angle)};
}

function setNewHighScore(data , callback){
	
	showPopUpName((name)=>{
		// save highScore
		data.userName = name;
		saveScore( data , callback)
	})
	
}
 
function lerp(v0, v1, t) {
    return (1 - t) * v0 + v1 * t;
}

function inverseLerp(v0, v1, val) {
    return   (val-v0) / (v1 - v0);
}

function clamp(min, max , val) {
  return Math.min(Math.max(val, min), max);
};