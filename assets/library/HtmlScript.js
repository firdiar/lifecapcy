// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

var mod1 = document.getElementById("mod1");
var mod2 = document.getElementById("mod2");
var mod3 = document.getElementById("mod3");

var inputUsername = document.getElementById("username");
var hint = document.getElementById("hint");
var hint2 = document.getElementById("hint2");

// Get the <span> element that closes the modal
var closeBtn = document.getElementsByClassName("close")[0];

var ceklis = document.getElementById("ceklis");

// When the user clicks the button, open the modal 

//btn.onclick = showPopUpName

var currentCallback = null;

function showLoading(){
  modal.style.display = "block";
  mod1.style.display = "none";
  mod2.style.display = "none";
  mod3.style.display = "block";
  hint.style.display = "none";
  hint2.style.display = "none";
  hint2.innerHTML = "";
}



function showPopUpName(callback) {
  modal.style.display = "block";
  mod1.style.display = "block";
  mod2.style.display = "none";
  mod3.style.display = "none";
  hint.style.display = "none";
  hint2.style.display = "none";
  hint2.innerHTML = "";
  closeBtn.disabled = false;
  
  currentCallback = callback;
  
}

// When the user clicks on <span> (x), close the modal
closeBtn.onclick = function() {
  
  if(inputUsername.value.length == 0 || inputUsername.value.length > 12){
	  console.log("error");
	  hint.style.display = "block";
	  return;
  }
  
  mod1.style.display = "none";
  mod3.style.display = "block";
  
  closeBtn.disabled = true;
  
  currentCallback(inputUsername.value);

}

function hideModal(){
	modal.style.display = "none";
	ceklis .innerHTML = ""
}

function showSuccess(msg){
	mod3.style.display = "none";
	mod2.style.display = "block";
	ceklis.innerHTML = "<img src='assets/htmlimage/ceklis.png' alt='success' width='50%' style='margin-bottom:10px'  />";
	//hint2.style.display = "block";
	//hint2.innerHTML = msg;
	setTimeout(hideModal, 2000)
}

function showFailed(error){
	mod3.style.display = "none";
	mod2.style.display = "block";
	ceklis.innerHTML = "<img src='assets/htmlimage/cross.png' alt='success' width='50%' style='margin-bottom:10px' />";
	hint2.style.display = "block";
	hint2.innerHTML = error;
	setTimeout(hideModal, 2000)
	
}

//showPopUpName(null)